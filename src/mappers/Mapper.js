import {SchemaMeta} from '../entities';

export default class Mapper {
    constructor(mappings) {
        this.mappings = mappings;
    }

    /**
     * 
     * @param {SchemaMeta} meta 
     */
    getSchema(meta) {
        if (!(meta instanceof SchemaMeta)) {
            throw new Error('Invalid Schema metadata')
        }

        if (!(meta.getType() in this.mappings)) {
            throw new Error('Invalid Schema type');
        }

        return this.mappings[meta.getType()](meta.getValue(), meta.getOptions(), meta.getDefault(), meta.isRequired());
    }
}