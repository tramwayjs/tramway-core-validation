export default class ValidationProvider {
    /**
     * 
     * @param {Entity} entity 
     * @param {Schema} schema 
     * @param {Object} options
     */
    validate(entity, schema, options = {}) {
        return entity;
    }
}