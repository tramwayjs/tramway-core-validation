export default class InvalidSchemaError extends Error {
    constructor(message, errors = {}) {
        super(message);
        this.errors = errors;
    }

    getErrors() {
        return this.errors;
    }

    getErrorKeys() {
        return Object.keys(this.errors);
    }
}