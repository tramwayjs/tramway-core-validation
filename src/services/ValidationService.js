export default class ValidationService {
    constructor(validationProvider, validationFactory) {
        this.validationProvider = validationProvider;
        this.validationFactory = validationFactory;
    }

    /**
     * 
     * @param {Entity} entity 
     * @param {Schema} schema 
     * @param {Object} options
     */
    validate(entity, schema, options = {}) {
        if (this.validationFactory) {
            schema = this.validationFactory.create(schema);
        }
        
        return this.validationProvider.validate(entity, schema, options);
    }
}