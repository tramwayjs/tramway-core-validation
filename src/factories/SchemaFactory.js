import {Schema} from '../entities';

export default class SchemaFactory {
    /**
     * 
     * @param {Mapper} mapper 
     */
    constructor(mapper) {
        this.mapper = mapper;
    }

    /**
     * 
     * @param {Schema} schema 
     */
    create(schema) {
        if ((schema instanceof Schema)) {
            throw new Error('Expected valid Schema');
        }

        for(let [key, schemaMeta] of Object.entries(schema)) {
            schema[key] = this.mapper.getSchema(schemaMeta);
        }

        return schema;
    }
}