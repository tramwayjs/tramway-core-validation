export default class Mappings {
    static REGEXP = 'regex';
    static RANGE = 'range';
    static INT_RANGE = 'intRange';
    static STRING = 'string';
    static NUMBER = 'number';
    static EMAIL = 'email';
    static ENTITY = 'entity';
    static CREDIT_CARD = 'creditCard';
    static TOKEN = 'token';
    static IP = 'ip';
    static URI = 'uri';
    static GUID = 'guid';
    static ISO_DATE = 'isoDate';
    static TIMESTAMP = 'timestamp';
    static DATE = 'date';
    static CUSTOM = 'custom';
}