export default class SchemaMeta {
    /**
     * 
     * @param {string} type The type of schema meta
     */
    constructor(type) {
        this.type = type;
        this.accompanies = [];
        this.excludes = [];
        this.params = {};
    }

    /**
     * 
     * @param {*} value 
     * @returns {SchemaMeta}
     */
    value(value) {
        this.value = value;
        return this;
    }

    /**
     * 
     * @returns {SchemaMeta}
     */
    required() {
        this.required = true;
        return this;
    }

    /**
     * 
     * @param {...string} keys Object keys that must be present with this one
     * @returns {SchemaMeta}
     */
    accompanies(...keys) {
        this.accompanies = keys;
        return this;
    }

    /**
     * 
     * @param {...string} keys Object keys that must not be present with this one
     * @returns {SchemaMeta}
     */
    excludes(...keys) {
        this.excludes = keys;
        return this;
    }

    /**
     * 
     * @param {Object} options Options associated to the value or type that the validator will use
     * @returns {SchemaMeta}
     */
    options(options = {}) {
        this.params = options;
        return this;
    }

    /**
     * 
     * @param {*} value 
     * @returns {SchemaMeta}
     */
    default(value) {
        this.defaultValue = value;
        return this;
    }

    hasOptions() {
        return this.params && Object.keys(this.params).length > 0;
    }

    getType() {
        return this.type;
    }

    getOptions() {
        return this.params;
    }

    getAccompanies() {
        return this.accompanies;
    }
    
    getExcludes() {
        return this.excludes;
    }

    getValue() {
        return this.value;
    }

    getDefault() {
        return this.defaultValue;
    }

    isRequired() {
        return this.required;
    }
}