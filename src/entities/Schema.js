import SchemaMeta from './SchemaMeta';
import {Mappings} from '../mappings';

export default class Schema {
    /**
     * 
     * @param {RegExp} regexp 
     * @returns {SchemaMeta}
     */
    static regex(regexp) {
        return new SchemaMeta(Mappings.REGEXP).value(regexp);
    }

    /**
     * 
     * @param {number} min 
     * @param {number} max 
     * @returns {SchemaMeta}
     */
    static range(min, max) {
        return new SchemaMeta(Mappings.RANGE).value([min, max]);
    }

    /**
     * 
     * @param {number} options.length Length of string
     * @param {number} options.minLength Min length of the string
     * @param {number} options.maxLength Max length of the string
     * @param {boolean} options.truncate Whether to truncate at the max length
     * @param {boolean} options.uppercase 
     * @param {boolean} options.lowercase
     * @returns {SchemaMeta}
     */
    static string({length, minLength, maxLength, truncate, uppercase, lowercase}) {
        return new SchemaMeta(Mappings.STRING).value(length).options({minLength, maxLength, truncate, uppercase, lowercase});
    }

    /**
     * @returns {SchemaMeta}
     */
    static number() {
        return new SchemaMeta(Mappings.NUMBER);
    }

    /**
     * 
     * @param {number} min 
     * @param {number} max 
     * @returns {SchemaMeta}
     */
    static intRange(min, max) {
        return new SchemaMeta(Mappings.INT_RANGE).value([min, max]);
    }

    /**
     * 
     * @param {Object} options 
     * @returns {SchemaMeta}
     */
    static email(options = {}) {
        return new SchemaMeta(Mappings.EMAIL).options(options);
    }

    /**
     * 
     * @param {string} entityName Name of the entity constructor
     * @returns {SchemaMeta}
     */
    static entity(entityName) {
        return new SchemaMeta(Mappings.ENTITY).value(entityName);
    }

    /**
     * @returns {SchemaMeta}
     */
    static creditCard() {
        return new SchemaMeta(Mappings.CREDIT_CARD);
    }

    /**
     * @returns {SchemaMeta}
     */
    static token() {
        return new SchemaMeta(Mappings.TOKEN);
    }

    /**
     * 
     * @param {string[]} options.version
     * @param {string} options.cidr
     * @returns {SchemaMeta} 
     */
    static ip(options = {}) {
        return new SchemaMeta(Mappings.IP).options(options);
    }

    /**
     * 
     * @param {string[] | RegExp[]}  options.scheme
     * @param {boolean} options.allowRelative
     * @param {boolean} options.relativeOnly
     * @param {boolean} options.allowQuerySquareBrackets
     * @returns {SchemaMeta}
     */
    static uri(options = {}) {
        return new SchemaMeta(Mappings.URI).options(options);
    }

    /**
     * 
     * @param {string[]} options.version Valid guid versions
     * @returns {SchemaMeta}
     */
    static guid(options = {}) {
        return new SchemaMeta(Mappings.GUID).options(options);
    }

    /**
     * @returns {SchemaMeta}
     */
    static isoDate() {
        return new SchemaMeta(Mappings.ISO_DATE);
    }

    /**
     * @returns {SchemaMeta}
     */
    static timestamp(type) {
        return new SchemaMeta(Mappings.TIMESTAMP).value(type);
    }

    /**
     * 
     * @param {Date} options.min 
     * @param {Date} options.max 
     * @param {Date} options.greater 
     * @param {Date} options.less 
     * @returns {SchemaMeta}
     */
    static date({min, max, greater, less}) {
        return new SchemaMeta(Mappings.DATE).options({min, max, greater, less});
    }

    /**
     * 
     * @param {*} value Custom schema for your provider
     * @returns {SchemaMeta}
     */
    static custom(value) {
        return new SchemaMeta(Mappings.CUSTOM).value(value);
    }
}